**Порядок установки Registry с самоподписным сертификатом**

- Склонировать проект в директорию /var/www/
- Установить Docker (найти в интернете или в конфе);
- Установка docker-compose
````
sudo apt install python-pip 

pip install docker-compose;
````
- Установить openssl

```
sudo apt install openssl
```
- В секцию [v3_ca] файла /etc/ssl/openssl.cnf нужно добавить subjectAltName = IP:ip_address
```
[ v3_ca ]
 
subjectAltName = IP:ip_address

```
- Сгенерировать самоподписной сертификат
```
openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -x509 -days 365 -out certs/domain.crt
```

- domain.crt в /etc/docker/certs.d/ip_address/ca.crt, если каких-то директорий не хватает, то нужно их создать. Сертификат нужно скопировать на все машины, где будет производиться работа с поднятым registry

- docker-compose up -d - зпуск registry

- docker login 192.168.1.2:5000 - аутентификация пользователя в registry

После этих действий будет доступна работа с поднятым registry.